fn main() {
    println!("Hello, control structures");

    // If and else
    let tru = 5 == 5;
    if tru {
        println!("  5 is 5");
    }

    let fls = 7 == 5;
    if fls {
        println!("  7 is 5");
    } else {
        println!("  7 is not 5");
    }

    let mut num = 2;
    if num <= 6 {
        println!("  2 is less than 7");
    } else if num >= 8 {
        println!("  2 is more than 7");
    } else {
        println!("  2 must be 7");
    }

    // Loops
    loop {
        if num > 0 {
            println!("  loop {}", num);
            num = num - 1;
        } else {
            break;
        }
    }

    while num < 2 {
        println!("  while {}", num);
        num = num + 1;
    }

    for num in 3..5 {
        println!("  for {}", num);
    }

    for wrd in ["for","arrays"].iter() {
        println!("  array for {}", wrd);
    }

    for (indx, wrd) in ["enumerate","elements"].iter().enumerate() {
        println!("  enumerate {} {}", indx, wrd);
    }

    // Labeled loops look bad... never do that

    let mut vector = vec![3, 7, 9];
    for num in &vector {
        println!("  vector {}", num);
    }

    let nine = vector.pop();
    vector.push(99);
    match nine{
        Some(x) => println!("  push and pop {}", x),
        None => println!("  none")
    }

    for num in 2..4 {
        match vector.get(num){
            Some(x) => println!("  vector.get {}", x),
            None => println!("  no panic, but no elem either")
        }
    }


    // If Let
    let three = Some(3);
    if let Some(x) = three {
        println!("  If Let found {}", x)
    }

    let none: Option<i64> = None;
    if let Some(_) = none {
        println!("  If Let found something")
    } else {
        println!("  If Let found nothing")
    }

    // While let
    let mut vec = vec![3, 7];
    while let Some(x) = vec.pop() {
        println!("  If While {}", x)
    }



}
